import axios from "axios";
import { NetworkConfigs } from "../utils/config";

export function getToken() {
  const token = document.cookie.match(/doit-token=([^ ;]*)/);
  if (token === null || token.length === 0) {
    return null;
  }

  return token[1];
}

export function setToken(token) {
  document.cookie = `doit-token=${token}; path=/`;
}

export const api = axios.create({
  baseURL: NetworkConfigs.BASE_URL
});

api.interceptors.request.use((request) => {
  const token = getToken();
  if (token) request.headers.Authorization = `Token ${token}`;
  return request;
});
