import { api, setToken } from "./api.js";

export const login = async (username, password) => {
  return await api.post("/login/", {
    username: username,
    password: password,
  })
    .then((response) => response.data);
}

export const performLogin = async (username, password) => {
  const response = await login(username, password);
  if (response === null) {
    return false;
  }

  const token = response.token;
  setToken(token);

  return true;
}
