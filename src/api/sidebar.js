import { api } from "./api.js";

export const getGroups = async () => {
  return await api.get("/todo/groups/")
    .then((response) => response.data);
}

export const getLists = async () => {
  return await api.get("/todo/lists/")
    .then((response) => response.data);
}

export const postGroup = async (name) => {
  return await api.post("/todo/groups/", {
    name: name
  })
    .then((response) => {
      console.log('r', response)
      return response.data
    });
}