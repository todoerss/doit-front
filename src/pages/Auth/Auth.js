import {
    NButton,
    NConfigProvider,
    NSpace,
    NInput,
    NGrid,
    NGi,
    NCard,
    NTabs,
    NTabPane,
    NAlert,
} from "naive-ui";
import { darkTheme } from "naive-ui";

import { performLogin } from "../../api/auth.js";

export default {
    components: {
        NConfigProvider,
        NButton,
        NSpace,
        NInput,
        NGrid,
        NGi,
        NCard,
        NTabs,
        NTabPane,
        NAlert,
    },
    data() {
        return {
            awaitingResponse: false,
            err: null,
            username: "",
            password: ""
        };
    },
    setup() {
        return {
            darkTheme,
        };
    },
    methods: {
        async login() {
            this.err = null;
            if (this.username != "" && this.password != "") {
                this.awaitingResponse = true;
                const response = await performLogin(this.username, this.password);
                this.awaitingResponse = false;
                response ? 
                    this.$router.push(`/todo-list`) : 
                    this.err = { description: "Failed :(" };
            } else {
                this.err = { description: "Failed :(" }
            }
        },
    },
};