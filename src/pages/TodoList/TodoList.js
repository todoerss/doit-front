import SidebarList from "../../components/SidebarList/SidebarList.vue";
import Dashboard from "../../components/Dashboard.vue";
import SidebarFooter from "../../components/SidebarFooter.vue";
import Footer from "../../components/Footer.vue";
import { getToken } from '../../api/api.js'

export default {
  name: "Page",
  components: {
    SidebarList,
    Dashboard,
    SidebarFooter,
    Footer
  },
  data() {
    return {
        authorised: false,
    };
  },
  setup() {
    const token = getToken();
    return {
      authorized: token ? true : false,
    };
  },
};
