import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/auth",
    name: "auth",
    component: () => import("../pages/Auth/Auth.vue"),
  },
  {
    path: "/todo-list",
    name: "todo",
    component: () => import("../pages/TodoList/TodoList.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
