import { NAlert, NInput, } from "naive-ui";
import { IconConfigProvider, Icon } from '@vicons/utils'
import { KeyboardArrowUpFilled } from '@vicons/material'
import { MoreHorizontal16Filled } from '@vicons/fluent'
import { postGroup, getGroups, getLists } from "../../api/sidebar.js";
import { PlusFilled } from '@vicons/material'

export default {
  name: "SidebarList",
  components: {
    NAlert,
    NInput,
    IconConfigProvider,
    Icon,
    KeyboardArrowUpFilled,
    MoreHorizontal16Filled,
    PlusFilled,
  },
  data() {
    return {
      groups: [],
      lists: [],
      isToggled: {},
      isMenu: {},
      addGroup: false,
      awaitingResponse: false,
      err: null,
      group: "",
    };
  },
  async created() {
    try {
      this.groups = await getGroups()
      this.groups.map((el) => this.isToggled[el.id] = false)
      this.groups.map((el) => this.isMenu[el.id] = false)
    } catch(e) {
      this.err = { description: e };
    }
    try {
      this.lists = await getLists()
    } catch(e) {
      this.err = { description: e };
    } 
  },
  methods: {
    async createGroup() {
      this.err = null;
      if (this.group != "") {
          this.awaitingResponse = true;
          const response = await postGroup(this.group);
          this.awaitingResponse = false;
          if (response) {
            this.groups.push(response)
            this.addGroup = false
          } else {
            this.err = { description: "Failed :(" };
          }
      } else {
          this.err = { description: "Failed :(" }
      }
    },
  },
};