describe("unit", () => {
  it("Can't login without password", () => {
    cy.visit("/auth");
    cy.findByTestId("log-in-username").click().type('test')
    cy.findByText("Log in").click()
    cy.findByText("Failed :(")
  });

  it("Can't login without username", () => {
    cy.reload()
    cy.findByTestId("log-in-password").click().type('test')
    cy.findByText("Log in").click()
    cy.findByText("Failed :(")
  });
})

describe("integration", () => {
  it("Log in when data is in the db", () => {
    cy.visit("/auth");
    cy.findByTestId("log-in-username").click().type('test')
    cy.findByTestId("log-in-password").click().type('test')
    cy.findByText("Log in").click()
  });
});

describe("view", () => {
  it("Log in is green when clicked", () => {
    cy.visit("/auth");
    cy.findByText("LOG IN").should('have.css', 'color', 'rgb(99, 226, 183)');
    cy.findByText("SIGN UP").should('have.css', 'color', 'rgba(255, 255, 255, 0.9)');
  });

  it("Input is green when click", () => {
    cy.findByTestId("log-in-username")
      .click()
      .should('have.css', 'color', 'rgba(255, 255, 255, 0.82)');
  });

  it("Sign up is green when clicked", () => {
    cy.findByText("SIGN UP").click().should('have.css', 'color', 'rgb(99, 226, 183)');
    cy.findByText("LOG IN").should('have.css', 'color', 'rgba(255, 255, 255, 0.9)');
  });
});
